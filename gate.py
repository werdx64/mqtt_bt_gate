import time
import select
import bluetooth
import threading
from paho.mqtt import client as mqtt_client

class Gate:
	def __init__(self):
		super().__init__()

		self.mqtt_broker = None
		self.mqtt_port = None

		self.mqtt_input_topic = None
		self.mqtt_output_topic = None

		self.mqtt_user = None
		self.mqtt_pass = None
		self.mqtt_id = None
		self.mqtt_qos = None
		self.mqtt_ret = None

		self.bt_mac = None
		self.bt_port = None
		self.bt_pin = None

		self.tag = None

		self.mqtt_client = None
		self.bt_client = None

		self.mqtt_connected = None
		self.bt_connected = None

		self.mqtt_status = None
		self.mqtt_msg_arrived = None
		self.mqtt_auth_en = None

		self.bt_data_ready = None
		self.bt_auth_en = None
		self.bt_data_buffer = None

		self.mqtt_msg = None

		self.is_configured = None
		self.is_started = None

	def connect_mqtt_cb(self, client, userdata, flags, rc):
		self.log("Connected to MQTT")
		if rc == 0:
			self.mqtt_connected = 1
			self.mqtt_status = rc
		else:
			self.mqtt_status = rc

	def disconnect_mqtt_cb(self, client, userdata, flags, rc):
		self.mqtt_connected = 0
		self.mqtt_status = rc
		return

	def on_message_cb(self, client, userdata, msg):
		self.mqtt_msg_arrived = 1
		if msg.topic == self.mqtt_input_topic:
			self.mqtt_msg = msg.payload.decode()
		return

	def connect_mqtt(self):
		self.mqtt_client = mqtt_client.Client(self.mqtt_id)

		if self.mqtt_auth_en == 1:
			self.mqtt_client.username_pw_set(self.mqtt_user, self.mqtt_pass)

		self.mqtt_client.on_connect = self.connect_mqtt_cb
		self.mqtt_client.on_disconnect = self.disconnect_mqtt_cb
		self.mqtt_client.on_message = self.on_message_cb
		self.mqtt_client.connect(self.mqtt_broker, self.mqtt_port)

		return

	def connect_bluetooth(self):
		self.bt_client = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
		self.bt_client.connect((self.bt_mac, self.bt_port))
		self.bt_connected = 1
		return

	def check_type(self, var, target_type):
		if str(type(target_type))[8:-2] != "str": raise TypeError
		if str(type(var))[8:-2] != target_type: raise TypeError
		return

	def configure(self):
		self.mqtt_timeout_precision = 0.5
		self.mqtt_timeout_precision_counter = 0
		self.bt_data_buffer = b""
		self.mqtt_auth_en = 0
		self.bt_auth_en = 0

		if self.mqtt_broker == None: raise Exception("NotSetError")
		if self.mqtt_port == None: raise Exception("NotSetError")
		if self.mqtt_input_topic == None: raise Exception("NotSetError")
		if self.mqtt_output_topic == None: raise Exception("NotSetError")
		if self.mqtt_user != None and self.mqtt_pass != None: self.mqtt_auth_en = 1
		if self.bt_pin != None: self.bt_auth_en = 1
		if self.mqtt_id == None: raise Exception("NotSetError")
		if self.mqtt_qos == None: raise Exception("NotSetError")
		if self.mqtt_ret == None: raise Exception("NotSetError")

		if self.bt_mac == None: raise Exception("NotSetError")
		if self.bt_port == None: raise Exception("NotSetError")

		if self.tag == None: raise Exception("NotSetError")

		self.check_type(self.mqtt_broker, "str")
		self.check_type(self.mqtt_port, "int")
		self.check_type(self.mqtt_input_topic, "str")
		self.check_type(self.mqtt_output_topic, "str")
		self.check_type(self.mqtt_id, "str")
		self.check_type(self.mqtt_qos, "int")
		self.check_type(self.mqtt_ret, "bool")

		self.check_type(self.bt_mac, "str")
		self.check_type(self.mqtt_port, "int")

		self.check_type(self.tag, "str")

		if self.mqtt_auth_en == 1:
			self.check_type(self.mqtt_user, "str")
			self.check_type(self.mqtt_pass, "str")

		if self.bt_auth_en == 1:
			self.check_type(self.bt_pin, "int")

		if self.mqtt_port > 65535 or self.mqtt_port < 0: raise ValueError
		if self.mqtt_qos > 2 or self.mqtt_qos < 0: raise ValueError

		if self.mqtt_auth_en == 1:
			if self.mqtt_user == None: raise Exception("NotSetError")
			if self.mqtt_pass == None: raise Exception("NotSetError")

			self.check_type(self.mqtt_user, "str")
			self.check_type(self.mqtt_pass, "str")

		self.is_configured = 1

		return

	def start(self):
		if self.is_configured != 1:
			raise Exception("NotConfiguredError")

		self.connect_mqtt()
		self.mqtt_client.subscribe(self.mqtt_input_topic, self.mqtt_qos)

		self.connect_bluetooth()

		self.is_started = 1

		return

	def loop(self):
		if self.is_started != 1:
			raise Exception("NotStartedError")

		self.mqtt_client.loop()
		self.bt_data_ready = select.select([self.bt_client], [], [], 0.01)

		if self.mqtt_connected != 1:
			self.log(self.mqtt_status)
			return

		if self.mqtt_msg_arrived == 1:
			self.mqtt_msg += "\r\n"
			self.bt_client.send(self.mqtt_msg)
			self.mqtt_msg_arrived = 0

		if self.bt_data_ready[0]:
			bt_data = self.bt_client.recv(1024)
			if bt_data[-1] != 10:
				self.bt_data_buffer += bt_data
			else:
				self.bt_data_buffer += bt_data
				self.mqtt_client.publish(self.mqtt_output_topic, self.bt_data_buffer, self.mqtt_qos, self.mqtt_ret)
				self.bt_data_buffer = b""

	def stop(self):
		self.bt_client.close()
		self.mqtt_client.disconnect()
		return

	def get_mqtt_status(self):
		return (self.mqtt_connected, self.mqtt_status)

	def log(self, msg):
		print(f"[{self.tag}]: {msg}")
		return
