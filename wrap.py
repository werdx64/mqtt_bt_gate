import sys
import os
import gate
import json
import atexit
import time
import threading

class Wrapper:
	def __init__(self):
		super().__init__()

		print('Reading config...')

		self.config_file = open('config.json', 'r')
		self.config_raw = self.config_file.read()
		self.config = json.loads(self.config_raw)
		self.config_file.close()

		self.ifaces = []
		self.tmpiclass = None

		print('Configuring class...')

		for i in self.config:
			self.tmpiclass = gate.Gate()

			for m in i.keys():
				if m == 'mqtt_broker': self.tmpiclass.mqtt_broker = i[m]
				elif m == 'mqtt_port': self.tmpiclass.mqtt_port = i[m]
				elif m == 'mqtt_input': self.tmpiclass.mqtt_input_topic = i[m]
				elif m == 'mqtt_output': self.tmpiclass.mqtt_output_topic = i[m]
				elif m == 'mqtt_username': self.tmpiclass.mqtt_user = i[m]
				elif m == 'mqtt_password': self.tmpiclass.mqtt_pass = i[m]
				elif m == 'mqtt_client_id': self.tmpiclass.mqtt_id = i[m]
				elif m == 'mqtt_qos': self.tmpiclass.mqtt_qos = i[m]
				elif m == 'mqtt_ret': self.tmpiclass.mqtt_ret = i[m]
				#elif m == 'mqtt_timeout': self.tmpiclass.mqtt_timeout = i[m]

				elif m == 'bt_address': self.tmpiclass.bt_mac = i[m]
				elif m == 'bt_port': self.tmpiclass.bt_port = i[m]

				elif m == 'tag': self.tmpiclass.tag = i[m]

			self.tmpiclass.configure()
			self.ifaces.append(self.tmpiclass)
			self.tmpiclass.log('Configured interface successfully')
			self.tmpiclass = None

	def start_ifaces(self):
		for i in self.ifaces:
			i.log("Starting interface...")
			i.start()
			i.log("Started interface successfully!")

		return

	def loop(self):
		for i in self.ifaces:
			i.loop()

	def onexit(self):
		os.system("killall bluetooth")
		sys.exit(0)

w = Wrapper()
w.start_ifaces()
#time.sleep(5)
while True: w.loop()
