# mqtt_bt_gate



# Description
This is a simple MQTT to Bluetooth gateway. Designed for usage
in embedded systems (SBCs).

# Files
There are 2 scripts: gate.py and wrap.py. First scripts contains gateway API.
Second one is a wrapper script. It starts your gateway according to config.json
file and manages multi-threading (software)

# Configuration file
The example configuration file is named config.json.

Available fields:

* MQTT broker hostname
* MQTT broker port
* MQTT input/output topics
* MQTT client ID
* MQTT QoS parameter
* Is MQTT messages retained

* MQTT broker username and password
* Bluetooth PIN

# Further improvement
New features will be added to this gateway randomly